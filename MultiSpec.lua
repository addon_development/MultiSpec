MultiSpec = {}
MultiSpec.DEBUG = false
MultiSpec.specToApply = nil
MultiSpec.buttons = {}

local f = CreateFrame("FRAME", "MultiSpecFrame")

-- container for buttons
local specButtons = {}

local oldToggleTalentFrame = ToggleTalentFrame

function MultiSpec:Print(str)
    if MultiSpec.DEBUG then
        print(str)
    end
end

local defaultButtonTexture = {}

function MultiSpec:ButtonInit() 

    local buttonSpec1 = nil
    local buttonSpec2 = nil
    local buttonSpec3 = nil
    

    ToggleTalentFrame = function(suggestedTab) 
        oldToggleTalentFrame(suggestedTab)

        if buttonSpec1 == nil then
            MultiSpec:Print("Creating Frames")
            MultiSpec:Print("attaching to: " .. tostring(TalentFrameScrollFrameScrollBarScrollUpButton))
            
            local parentFrame = TalentFrameScrollFrameScrollBarScrollUpButton

            buttonSpec1 = CreateFrame("Button", "spec_1_button", parentFrame, "UIPanelButtonTemplate")
            defaultButtonTexture.normal = buttonSpec1:GetNormalTexture()
            defaultButtonTexture.highlighted = buttonSpec1:GetHighlightTexture()
            defaultButtonTexture.disabled = buttonSpec1:GetDisabledTexture()
            buttonSpec1:SetPoint("TOPLEFT", parentFrame, "TOPRIGHT", 10, 0)
            buttonSpec1:SetWidth(36)
            buttonSpec1:SetHeight(36)
            buttonSpec1.Text:SetPoint("TOPLEFT", buttonSpec1, "BOTTOMLEFT")
            buttonSpec1:Show()

            local closeOne = CreateFrame("Button", "spec_1_button_close", buttonSpec1, "UIPanelCloseButton")
            buttonSpec1.closeButton = closeOne
            closeOne:SetPoint("TOPRIGHT", buttonSpec1, "TOPRIGHT", 5, 5)
            closeOne:SetWidth(20)
            closeOne:SetHeight(20)
            closeOne:SetScript("OnClick", function() 
                if buttonSpec1:IsEnabled() == false then return end
                MultiSpec:AttemptToResetTalentsFor(1)
            end)
            
        
            buttonSpec2 = CreateFrame("Button", "spec_2_button", buttonSpec1, "UIPanelButtonTemplate")
            buttonSpec2:SetPoint("TOPLEFT", buttonSpec1, "BOTTOMLEFT", 0, -20)
            buttonSpec2:SetWidth(36)
            buttonSpec2:SetHeight(36)
            buttonSpec2.Text:SetPoint("TOPLEFT", buttonSpec2, "BOTTOMLEFT")
            buttonSpec2:Show()

            local closeTwo = CreateFrame("Button", "spec_2_button_close", buttonSpec2, "UIPanelCloseButton")
            buttonSpec2.closeButton = closeTwo
            closeTwo:SetPoint("TOPRIGHT", buttonSpec2, "TOPRIGHT", 5, 5)
            closeTwo:SetWidth(20)
            closeTwo:SetHeight(20)
            closeTwo:SetScript("OnClick", function()
                if buttonSpec2:IsEnabled() == false then return end 
                MultiSpec:AttemptToResetTalentsFor(2)
            end)
            
        
            buttonSpec3 = CreateFrame("Button", "spec_3_button", buttonSpec2, "UIPanelButtonTemplate")
            buttonSpec3:SetPoint("TOPLEFT", buttonSpec2, "BOTTOMLEFT", 0, -20)
            buttonSpec3:SetWidth(36)
            buttonSpec3:SetHeight(36)
            buttonSpec3.Text:SetPoint("TOPLEFT", buttonSpec3, "BOTTOMLEFT")
            buttonSpec3:Show()

            local closeThree = CreateFrame("Button", "spec_3_button_close", buttonSpec3, "UIPanelCloseButton")
            buttonSpec3.closeButton = closeThree
            closeThree:SetPoint("TOPRIGHT", buttonSpec3, "TOPRIGHT", 5, 5)
            closeThree:SetWidth(20)
            closeThree:SetHeight(20)
            closeThree:SetScript("OnClick", function() 
                if buttonSpec3:IsEnabled() == false then return end
                MultiSpec:AttemptToResetTalentsFor(3)
            end)
            

            MultiSpec.buttons[1] = buttonSpec1
            MultiSpec.buttons[2] = buttonSpec2
            MultiSpec.buttons[3] = buttonSpec3

            MultiSpec:UpdateAllButtons()

            parentFrame:SetScript("OnShow", function()
                MultiSpec:UpdateAllButtons()
            end)
        end
    end
end
---------- END BUTTON INIT ----------
function MultiSpec:UpdateAllButtons()
    MultiSpec:UpdateButton(MultiSpec.buttons[1], 1)
    MultiSpec:UpdateButton(MultiSpec.buttons[2], 2)
    MultiSpec:UpdateButton(MultiSpec.buttons[3], 3)
end

function MultiSpec:UpdateButton(button, spec) 
    local treeOne, treeTwo, treeThree = GetLastKnownPlayerSpecFor(spec)
    button:SetEnabled(not (spec == MultiSpecLastActiveSpec))
    
    if button:IsEnabled() then
        button.closeButton:Show()
    else
        button.closeButton:Hide()
        button:SetDisabledFontObject("GameFontNormal")
    end

    local icon = MultiSpec:GetIconFor(spec)
    if icon then 
        MultiSpec:Print("setting texture of button to: " .. tostring(icon))
        button:SetNormalTexture(icon);
        button:SetDisabledTexture(icon);
        local texture = button:GetNormalTexture()
        if button:IsEnabled() then
            texture:SetDesaturated(true)
            button:SetNormalTexture(texture);
        end

        button.Text:ClearAllPoints()
        button.Text:SetPoint("TOPLEFT", button, "BOTTOMLEFT")
        button:SetText(tostring(treeOne).."/"..tostring(treeTwo).."/"..tostring(treeThree)) 
    else
        -- No icon found, use + icon instead
        button:SetNormalTexture(defaultButtonTexture.normal)
        button:SetDisabledTexture(defaultButtonTexture.disabled)
        button:SetSize(20, 20)
        button:SetText("+")
        button.Text:ClearAllPoints()
        button.Text:SetPoint("CENTER", button, "CENTER")
        button.closeButton:Hide()
    end
    
    button:SetScript("OnClick", function(self, arg1)
        specToApply = spec
        MultiSpec:Print("attempting to apply spec: " .. tostring(spec))

        if specToApply == MultiSpecLastActiveSpec then 
            specToApply = nil
            StaticPopup_Show ("MULTI_SPEC_ALREADY_CURRENT")
            return
        end

        if not CheckTalentMasterDist() then
            StaticPopup_Show ("MULTI_SPEC_OUT_OF_RANGE")
            return
        end
        StaticPopup_Show ("APPLY_SPEC")
    end)
end

function MultiSpec:GetIconFor(spec)
 
    local one, two, three = GetLastKnownPlayerSpecFor(spec)
 
    if one == 0 and two == 0 and three == 0 then return nil end

    local treeWithMostPoints = -1
    local mostPoints = -1
    if one > two then 
        treeWithMostPoints = 1
        mostPoints = one
    else 
        treeWithMostPoints = 2
        mostPoints = 2
    end

    if mostPoints < three then 
        treeWithMostPoints = 3
    end
    
    local icon = nil
    local name = nil
    for i, row in pairs(MultiSpecTalentPoints[spec][treeWithMostPoints]) do 
        local talentName, talentIcon = GetTalentInfo(treeWithMostPoints,i);
        icon = talentIcon
        name = talentName
    end
   
    MultiSpec:Print("talent to return: " .. name.. " with icon: " .. icon)
    return icon
end

function MultiSpec:AttemptToResetTalentsFor(spec)
    StaticPopupDialogs["MULTI_SPEC_CONFIRM_CLEAR_TALENTS"].OnAccept = function()
        StoreEmptySpecFor(spec)
        MultiSpec:UpdateAllButtons()
    end

    StaticPopup_Show("MULTI_SPEC_CONFIRM_CLEAR_TALENTS")
end

local storeTalentsLock = false



-- There is no good way to see in game when a user has finally selected to respec.
-- We can however watch for the talent point change event and maintain a flag.
local talentChangeListenerFrame = CreateFrame("FRAME")

talentChangeListenerFrame:SetScript("OnEvent", function(self, event, ...)
    TalentsDidChange()
end)

function TalentsDidChange() 
    MultiSpec:Print("TalentsDidChange")
    if storeTalentsLock == true then
        ApplySpec(MultiSpecLastActiveSpec)

        local one, two, three = GetLastKnownPlayerSpecFor(MultiSpecLastActiveSpec)

        local totalPointsSpent = one + two + three
        local pointsLeftToSpend = UnitCharacterPoints("player")
        local playerTotalTalentPoints = UnitLevel("player") - 9

        if (playerTotalTalentPoints - totalPointsSpent == pointsLeftToSpend)  then 
            ActionBarSaver:RestoreProfile(ABSProfileName(MultiSpecLastActiveSpec))
            storeTalentsLock = false
        end

        MultiSpec:UpdateAllButtons()
        return 
    end

    SaveCurrentSpec(MultiSpecLastActiveSpec)
    MultiSpec:UpdateAllButtons()
end



-- Main on load function
function MultiSpec_OnLoad(self)
    -- Load from saved variables
    if MultiSpecTalentPoints == nil then
        MultiSpecTalentPoints = {}
        SaveCurrentSpec(1)
        StoreEmptySpecFor(2)
        StoreEmptySpecFor(3)
    end

    -- Load from saved variables
    if MultiSpecLastActiveSpec == nil then
        MultiSpecLastActiveSpec =  1
    end

    MultiSpec:ButtonInit()

    

    local actionBarListener = CreateFrame("FRAME")

    actionBarListener:SetScript("OnEvent", function(self, event, ...)
        if storeTalentsLock == true then return end
        ActionBarSaver:SaveProfile(ABSProfileName(MultiSpecLastActiveSpec))
    end)
    actionBarListener:RegisterEvent("ACTIONBAR_SLOT_CHANGED")
    talentChangeListenerFrame:RegisterEvent("CHARACTER_POINTS_CHANGED")
	
	DEFAULT_CHAT_FRAME:AddMessage("MultiSpec Loaded")
end

-- Takes a number which is then used to check against the saved specs. Currently specs 1-3 are all that are supported. 
function GetLastKnownPlayerSpecFor(spec)
    local talents = {0,0,0}

    if MultiSpecTalentPoints[spec] == nil then 
        return 0, 0, 0
    end
 
    for t=1, GetNumTalentTabs() do
        for i, row in pairs(MultiSpecTalentPoints[spec][t]) do 
            talents[t] = talents[t] + row
            
            nameTalent, icon, tier, column, currRank, maxRank= GetTalentInfo(t,i);
        end
    end
   
    return talents[1], talents[2], talents[3]
end

function SaveCurrentSpec(spec)
    StoreCurrentTalentPointsFor(spec)
    ActionBarSaver:SaveProfile(ABSProfileName(spec))
end

-- Takes a number and then saves the current talents to that number in our saved vars. Currently specs 1-3 are all that are supported. 
function StoreCurrentTalentPointsFor(spec) 
    local numTabs = GetNumTalentTabs();
    MultiSpecTalentPoints[spec] = {}
    for t=1, numTabs do
        MultiSpecTalentPoints[spec][t] = {}
        local numTalents = GetNumTalents(t);
        for i=1, numTalents do
            nameTalent, icon, tier, column, currRank, maxRank= GetTalentInfo(t,i);
            MultiSpecTalentPoints[spec][t][i] = currRank
        end
    end
end

-- Takes a number and then saves the current talents to that number in our saved vars. Currently specs 1-3 are all that are supported. 
function StoreEmptySpecFor(spec) 
    local numTabs = GetNumTalentTabs();
    MultiSpecTalentPoints[spec] = {}
    for t=1, numTabs do
        MultiSpecTalentPoints[spec][t] = {}
        local numTalents = GetNumTalents(t);
        for i=1, numTalents do
            nameTalent, icon, tier, column, currRank, maxRank= GetTalentInfo(t,i);
            MultiSpecTalentPoints[spec][t][i] = 0
        end
    end

    ActionBarSaver:SaveProfile(ABSProfileName(spec))
end

-- Takes a number and then applies those talents to our user. This function does not learn all talents in a single pass.
-- This function needs to be repeatedly called while listening to the CHARACTER_POINTS_CHANGED Event. see TalentsDidChange() 
-- Currently specs 1-3 are all that are supported.
function ApplySpec(spec) 
    local numTabs = GetNumTalentTabs();
    if MultiSpecTalentPoints[spec] == nil then
        return 
    end

    for t=1, numTabs do 
        local numTalents = GetNumTalents(t);
        
        for i=1, numTalents do
            local nameTalent, icon, tier, column, currRank, maxRank= GetTalentInfo(t,i);
            for numRanksToLearn=currRank, MultiSpecTalentPoints[spec][t][i] - 1 do
                LearnTalent(t, i)
            end
        end
    end
end

function ABSProfileName(spec)
    return "" .. UnitName("player") .. "_" .. UnitClass("player") .. "_" .. tostring(spec)
end


StaticPopupDialogs["MULTI_SPEC_OUT_OF_RANGE"] = {
    text = "Stand near your trainer to activate a new spec.",
    button1 = "Okay",
    timeout = 0,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3,  -- avoid some UI taint, see http://www.wowace.com/announcements/how-to-avoid-some-ui-taint/
  }

  StaticPopupDialogs["MULTI_SPEC_ALREADY_CURRENT"] = {
    text = "This is your current spec!",
    button1 = "Okay",
    timeout = 0,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3,  -- avoid some UI taint, see http://www.wowace.com/announcements/how-to-avoid-some-ui-taint/
  }

  StaticPopupDialogs["MULTI_SPEC_CONFIRM_CLEAR_TALENTS"] = {
    text = "Are you sure you want to reset the talent tree for this spec?",
    button1 = "Yes",
    button2 = "No",
    OnHide = function(self) 
        self.OnAccept = function() end -- simply reset this so there isnt a leaked reset spec function
    end,
    timeout = 0,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3,  -- avoid some UI taint, see http://www.wowace.com/announcements/how-to-avoid-some-ui-taint/
  }


StaticPopupDialogs["APPLY_SPEC"] = {
    text = "Do you want to apply new spec for",
    button1 = "Yes",
    button2 = "No",
    OnAccept = function()
        if specToApply == nil then 
            print("MultiSpec: Couldn't apply spec - Invalid spec ["..specToApply.."] selected.")
            return
        end
        if not CheckTalentMasterDist() then
            print("MultiSpec: Couldn't apply spec - You are too far from trainer.")
            MultiSpec:Print("Reason - CheckTalentMasterDist(): " .. tostring(CheckTalentMasterDist()))
            return
        end 

        if MultiSpec.DEBUG == true then
            MultiSpec:Print("ConfirmTalentWipe about to happen but was skipped") 
            return
        end

        ConfirmTalentWipe()
        MultiSpecLastActiveSpec = specToApply
        specToApply = nil
        storeTalentsLock = true
    end,
    OnCancel = function()
        specToApply = nil
    end,
    OnHide = function()
        specToApply = nil
    end,
    OnShow = function(self)
        MoneyFrame_Update(self.moneyFrame, 500000);
    end,
    hasMoneyFrame = 1,
    timeout = 0,
    whileDead = true,
    hideOnEscape = true,
    preferredIndex = 3,  -- avoid some UI taint, see http://www.wowace.com/announcements/how-to-avoid-some-ui-taint/
  }

------------- BEGIN SLASH COMMANDS ---------------
SLASH_MS1 = "/MS"
SLASH_MS2 = "/ms"
SlashCmdList["MS"] = function(msg)
   if string.lower(msg) == 'debug' then
    MultiSpec.DEBUG = not MultiSpec.DEBUG
    print("MS DEBUG - " .. tostring(MultiSpec.DEBUG))
   else
    print("MS commands:")
    print("DEBUG - Toggles debug printing")
   end
end 
------------- END SLASH COMMANDS -----------------

local addonLoadingFrame = CreateFrame("Frame");
local loadingEvent = "PLAYER_ENTERING_WORLD"
addonLoadingFrame:RegisterEvent(loadingEvent);
addonLoadingFrame:SetScript("OnEvent", function(__, event, arg1)
    if (event == loadingEvent) then
        MultiSpec_OnLoad(f);
        addonLoadingFrame:UnregisterEvent(loadingEvent); -- only need this to fire once
    end
end);

