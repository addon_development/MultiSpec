## Interface: 11302
## Author: Tandoor
## Title: MultiSpec
## Notes: Alternative to not having multi-spec in classic wow
## Version: 0.1
## OptionalDeps: 
## SavedVariables: ActionBarSaverDB
## SavedVariablesPerCharacter: MultiSpecTalentPoints, MultiSpecLastActiveSpec

MultiSpec.lua

localization\enUS.lua
localization\deDE.lua
localization\esES.lua
localization\esMX.lua
localization\frFR.lua
localization\koKR.lua
localization\ruRU.lua
localization\zhCN.lua
localization\zhTW.lua
ActionBarSaver.lua